------------------------------------------------------------
*		Remote Port forwarding:
------------------------------------------------------------
Example:
    	HTTP port forward over ssh:
   
   	Assuming bob has ssh server running.

    	Suppose alice wants to give access of locally running 
	http server to bob. To do that she can ustilize "remote 
	port forwarding over ssh".

	                                   <192.168.43.254>
   		    		                 [bob]	  
            ________________NETWORK________________|	 	
   	   |						  
    	[alice]						 
 <locally running http server>				
 <192.168.43.1 on 8080>				
			-----fig 1-----

        Alice will do launch:

	    ssh -N -R [pb]:localhost:[pa] bob@192.168.43.254

    	+explaination: 
	    	-N: do not execute remote command  
	    	-R: remote binding 
    		[pb]: port remote listens on(bob's computer)
    		[pa]: port localhost listen's on(alice's
	    		computer.
    		(you can add -f to put this process in back-
	    	ground).

	    ssh -N -R 9080:localhost:8080 bob@192.168.43.254

    	Now bob can access the alice's server on port 9080 on
    	his computer.

        Now bob can access alice's running service on his
        computer on port 9080.

note: 
	- You can do remote forwarding with for any service.
	- any connection given to remote(bob) port is
	  forwarded to local(alice) port.

---------------------end note-------------------------------
