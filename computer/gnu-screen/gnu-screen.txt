------------------------------------------------------------
*			GNU screen:
------------------------------------------------------------
Gnu screen is a terminal multiplexer. You can find a nice 
screenrc written by Brad Sims at https://ia601407.us.archi
ve.org/15/items/screenrc/screenrc.txt. 

*Command line options:
	-ls 	list sessions
	-S	start screen session with given name
	-r 	reattach screen session
	-x 	reattach an attach screen session without 
		detaching the attached session

*Attach screen sessions:
	
	$ screen -r sessionname

*Default keybindings:

	^a Esc  copy mode and "scroll" with jklh or scroll
		with mouse
	^a :	enter command mode
	^a d	detach session

*Listing all screen sessions:
	
	$ screen -ls
	
*Named sessions:
	To start a named screen session start screen '-S'
	followed by desired session name. 
	example:
	
	$ screen -S mysession

*Renaming a session:
	To rename a session enter the command mode by pressing
	'^a :' and using commmand "sessionname desiredname".

------------------------------------------------------------
